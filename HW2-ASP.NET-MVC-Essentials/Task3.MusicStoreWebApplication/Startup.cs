﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Task3.MusicStoreWebApplication.Startup))]
namespace Task3.MusicStoreWebApplication
{
    public partial class Startup 
    {
        public void Configuration(IAppBuilder app) 
        {
            ConfigureAuth(app);
        }
    }
}
