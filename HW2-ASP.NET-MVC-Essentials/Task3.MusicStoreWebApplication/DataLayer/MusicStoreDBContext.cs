﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Task3.MusicStoreWebApplication.Models;

namespace Task3.MusicStoreWebApplication.DataLayer
{
    public class MusicStoreDBContext: IdentityDbContext<User, UserClaim, UserSecret, UserLogin, Role, UserRole, Token, UserManagement>
    {
        public MusicStoreDBContext()
            : base("MusicStoreDB")
        {

        }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Genre> Genres { get; set; }
    }
}