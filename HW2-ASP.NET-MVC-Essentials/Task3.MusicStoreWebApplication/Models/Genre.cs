﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Task3.MusicStoreWebApplication.Models
{
    public class Genre
    {
        [ScaffoldColumn(false)]
        public int GenreId { get; set; }

        [Required(ErrorMessage = "Genre name is required")]
        [StringLength(60)]
        public string Name { get; set; }

        [Column(TypeName = "ntext")]
        public string Description { get; set; }
        public List<Album> Albums { get; set; }
    }
}