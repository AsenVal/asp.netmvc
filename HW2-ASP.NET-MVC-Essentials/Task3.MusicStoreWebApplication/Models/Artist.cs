﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Task3.MusicStoreWebApplication.Models
{
    public class Artist
    {
        [ScaffoldColumn(false)]
        public int ArtistId { get; set; }

        [Required(ErrorMessage = "Genre name is required")]
        [StringLength(100)]
        public string Name { get; set; }
    }
}