﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Task3.MusicStoreWebApplication.DataLayer;

namespace Task3.MusicStoreWebApplication.Controllers
{
    public class StoreController:Controller
    {
        private MusicStoreDBContext musicStoreDb;
        public StoreController()
        {
            musicStoreDb = new MusicStoreDBContext();
        }
        public ActionResult Details(int id)
        {
            var album = musicStoreDb.Albums.Include(x => x.Genre).Single(x => x.AlbumId == id);
            return View(album);
        }

        public ActionResult Browse(string genre)
        {
            var genreModel = musicStoreDb.Genres.Include(x=>x.Albums).FirstOrDefault(x => x.Name == genre);
            return View(genreModel);
        }

        public ActionResult Index()
        {
            var genres = musicStoreDb.Genres.ToList();
            return View(genres);
        }
    }
}