namespace Task3.MusicStoreWebApplication.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Task3.MusicStoreWebApplication.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Task3.MusicStoreWebApplication.DataLayer.MusicStoreDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Task3.MusicStoreWebApplication.DataLayer.MusicStoreDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            var isGenre = context.Genres.FirstOrDefault(x => x.Name == "Rock");
            if (isGenre == null)
            {
                context.Genres.AddOrUpdate(new Genre
                {
                    Name = "Rock"
                });

                context.Genres.AddOrUpdate(new Genre
                {
                    Name = "Metal"
                });

                context.Genres.AddOrUpdate(new Genre
                {
                    Name = "Jazz"
                });

                context.Genres.AddOrUpdate(new Genre
                {
                    Name = "Disco"
                });

                context.Genres.AddOrUpdate(new Genre
                {
                    Name = "Latin"
                });

                context.Genres.AddOrUpdate(new Genre
                {
                    Name = "Dance"
                });

                context.Genres.AddOrUpdate(new Genre
                {
                    Name = "Classical"
                });
                context.SaveChanges();
            }

            var isArtist = context.Artists.FirstOrDefault(x => x.Name == "Man of war");
            if (isArtist == null)
            {
                context.Artists.AddOrUpdate(new Artist
                {
                    Name = "Man of war"
                });

                context.Artists.AddOrUpdate(new Artist
                {
                    Name = "Aerosmith"
                });

                context.Artists.AddOrUpdate(new Artist
                {
                    Name = "Deep Purple"
                });

                context.SaveChanges();
            }

            var isAlbum = context.Albums.FirstOrDefault(x => x.Title == "The best of Man of war");
            if (isAlbum == null)
            {
                context.Albums.AddOrUpdate(new Album
                {
                    Title = "The best of Man of war",
                    Genre = context.Genres.FirstOrDefault(x => x.Name == "Rock"),
                    Artist = context.Artists.FirstOrDefault(x => x.Name == "Man of war"),
                    Price = 9.98m
                });

                context.Albums.AddOrUpdate(new Album
                {
                    Title = "Come taste the band",
                    Genre = context.Genres.FirstOrDefault(x => x.Name == "Rock"),
                    Artist = context.Artists.FirstOrDefault(x => x.Name == "Deep Purple"),
                    Price = 9.98m
                });

                context.SaveChanges();
            }
        }
    }
}
