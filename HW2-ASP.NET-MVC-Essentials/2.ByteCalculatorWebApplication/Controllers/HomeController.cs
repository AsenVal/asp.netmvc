﻿using _2.ByteCalculatorWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _2.ByteCalculatorWebApplication.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
            this.Types = GetBitList();
            this.TypesItems = new List<SelectListItem>();
            foreach (var item in this.Types)
            {
                this.TypesItems.Add(new SelectListItem() { Text = item.Name, Value = item.Name });
            }

            TypesItems[5].Selected = true;

            this.Kilo = new List<SelectListItem>(){
            new SelectListItem(){ Text="1024", Value="1024"},
            new SelectListItem(){ Text="1000", Value="1000"}
        };
        }

        public List<Bit> Types { get; set; }
        public List<SelectListItem> TypesItems { get; private set; }
        public List<SelectListItem> Kilo { get; private set; }

        [HttpGet]
        public ActionResult BitCalculator()
        {
            this.ViewBag.Quantity = 1;
            this.ViewBag.Types = this.TypesItems;
            this.ViewBag.Kilo = this.Kilo;
            this.ViewBag.Results = this.Types;
            return View();
        }

        [HttpPost]
        public ActionResult BitCalculator(string Quantity, string Types, string Kilo)
        {
            this.ViewBag.Quantity = Quantity;
            var type = this.TypesItems.Single(x => x.Value == Types);
            type.Selected = true;
            this.ViewBag.Types = this.TypesItems;

            var kilo = this.Kilo.Single(x => x.Value == Kilo);
            kilo.Selected = true;
            this.ViewBag.Kilo = this.Kilo;

            var indexOfItem = this.Types.IndexOf(this.Types.Single(x => x.Name == Types));
            int kiloKoef = int.Parse(Kilo);
            bool IsBit = Types.Contains("bit") ? true : false;
            double quantity = Convert.ToDouble(Quantity);
            //double tempQuantity = quantity;

            this.Types[indexOfItem].Value = 1;
            //before selected item
            for (int i = indexOfItem; i >= 1;)
            {
                var previoisValue = this.Types[i].Value;
                if (IsBit)
                {
                    this.Types[i - 1].Value = previoisValue * kiloKoef / 8;
                    if (i - 2 >= 0)
                    {
                        this.Types[i - 2].Value = previoisValue * kiloKoef;
                    }
                }
                else
                {
                    this.Types[i - 1].Value = previoisValue * 8;
                    if (i - 2 >= 0)
                    {
                        this.Types[i - 2].Value = previoisValue * kiloKoef;
                    }
                }

                i-=2;
            }

            //after selected item
            for (int i = indexOfItem; i < this.Types.Count - 1; )
            {
                var previoisValue = this.Types[i].Value;
                if (IsBit)
                {
                    this.Types[i + 1].Value = previoisValue / 8;
                    if (i + 2 < this.Types[i].Value)
                    {
                        this.Types[i + 2].Value = previoisValue / kiloKoef / 8;
                    }
                }
                else
                {
                    this.Types[i + 1].Value = previoisValue / kiloKoef * 8;
                    if (i + 2 < this.Types[i].Value)
                    {
                        this.Types[i + 2].Value = previoisValue / kiloKoef;
                    }
                }

                i += 2;
            }

            this.ViewBag.Results = this.Types;

            return View();
        }

        public List<Bit> GetBitList()
        {
            var bitList = new List<Bit>()
            {
                new Bit(){ Name = "Bit - b", Value = 8388608},
                new Bit(){ Name = "Byte - B", Value = 1048576},
                new Bit(){ Name = "Kilobit - Kb", Value = 8192},
                new Bit(){ Name = "Kilobyte - KB", Value = 1024},
                new Bit(){ Name = "Megabit - Mb", Value = 8},
                new Bit(){ Name = "Megabyte - MB", Value = 1},
                new Bit(){ Name = "Gigabit - Gb", Value = 0.0078125m},
                new Bit(){ Name = "Gigabyte - GB", Value = 0.0009765625m},
                new Bit(){ Name = "Terabit - Tb", Value = 7.62939453125E-6m},
                new Bit(){ Name = "Terabyte - TB", Value = 9.5367431640625E-7m},
                new Bit(){ Name = "Petabit - Pb", Value = 7.4505805969238E-9m},
                new Bit(){ Name = "Petabyte - PB", Value = 9.3132257461548E-10m},
                new Bit(){ Name = "Exabit - Eb", Value = 7.2759576141834E-12m},
                new Bit(){ Name = "Exabyte - EB", Value = 9.0949470177293E-13m},
                new Bit(){ Name = "Zettabit - Zb", Value = 7.105427357601E-15m},
                new Bit(){ Name = "Zettabyte - ZB", Value = 8.8817841970013E-16m},
                new Bit(){ Name = "Yottabit - Yb", Value = 6.9388939039072E-18m},
                new Bit(){ Name = "Yottabyte - YB", Value = 8.673617379884E-19m},
            };
            return bitList;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}