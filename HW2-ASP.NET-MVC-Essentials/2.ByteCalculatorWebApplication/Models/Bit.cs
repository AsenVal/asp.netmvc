﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _2.ByteCalculatorWebApplication.Models
{
    public class Bit
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
    }
}