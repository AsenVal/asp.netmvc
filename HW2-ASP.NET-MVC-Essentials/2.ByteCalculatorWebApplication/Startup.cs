﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_2.ByteCalculatorWebApplication.Startup))]
namespace _2.ByteCalculatorWebApplication
{
    public partial class Startup 
    {
        public void Configuration(IAppBuilder app) 
        {
            ConfigureAuth(app);
        }
    }
}
